import Button from "../button";
import {
  addToCartModalDesc,
  deleteFromCartModalDesc,
} from "../../mock/modalDescriptions";
import { closeModal } from "../../redux/actions/modal";
import { addToCart, deleteFromCart } from "../../redux/actions/cart";
import { useDispatch } from "react-redux";

export default function ToggleCartButton(props) {
  const dispatch = useDispatch();
  return props.inCart ? (
    <Button
      backgroundColor="#0A84FF"
      text="Delete from cart"
      onClick={() =>
        function () {
          props.createModal(deleteFromCartModalDesc, [
            function () {
              dispatch(closeModal());
            },
            function () {
              dispatch(closeModal());
              dispatch(deleteFromCart(props.article));
            },
          ]);
        }
      }
    />
  ) : (
    <Button
      backgroundColor="#0A84FF"
      text="Add to cart"
      onClick={() =>
        function () {
          props.createModal(addToCartModalDesc, [
            function () {
              dispatch(closeModal());
            },
            function () {
              dispatch(closeModal());
              dispatch(addToCart(props.article));
            },
          ]);
        }
      }
    />
  );
}
