import styles from "./Header.module.scss";
import logo from "../../img/logo.png";
import { NavLink } from "react-router-dom";
import StarIcon from "../starIcon";
import CartIcon from "../cartIcon";
import { useSelector } from "react-redux";

export default function Header() {
  let { inCart } = useSelector((state) => state.cart);
  let { inFavorites } = useSelector((state) => state.favorites);

  return (
    <header className={styles.header}>
      <nav className={`container ${styles.container}`}>
        <NavLink to={"/"} className={`${styles.logo} ${styles.link}`}>
          <img className={styles.logoImg} src={logo} alt="logo" />
          <span className={styles.logoText}>Apple goods</span>
        </NavLink>
        <div className={styles.pagesWrap}>
          <NavLink to="/" className={styles.link}>
            Home
          </NavLink>
          <NavLink to="/cart" className={styles.link}>
            Cart
          </NavLink>
          <NavLink to="/favorites" className={styles.link}>
            Favorites
          </NavLink>
        </div>
        <div className={styles.iconsWrap}>
          <NavLink to={"/cart"} className={styles.cart}>
            <CartIcon className={styles.cartIcon} color="#A6CE39" />
            <span className={styles.cartCount}>{inCart.length}</span>
          </NavLink>
          <NavLink to={"/favorites"} className={styles.favorites}>
            <StarIcon
              fill={true}
              className={styles.favoritesIcon}
              color="#A6CE39"
            />
            <span className={styles.favoritesCount}>{inFavorites.length}</span>
          </NavLink>
        </div>
      </nav>
    </header>
  );
}
