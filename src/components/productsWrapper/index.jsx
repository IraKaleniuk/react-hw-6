import { useDispatch, useSelector } from "react-redux";
import { useContext } from "react";
import { ThemeContext } from "../../context/themeContext";
import ProductList from "../productList";
import ProductTable from "../productTable";
import PropTypes from "prop-types";
import {
  addToFavorites,
  deleteFromFavorites,
} from "../../redux/actions/favorites";
import Button from "../button";
import { openModal } from "../../redux/actions/modal";

export default function ProductsWrapper(props) {
  let { products } = useSelector((state) => state.products);
  const { inCart } = useSelector((state) => state.cart);
  const { inFavorites } = useSelector((state) => state.favorites);
  const dispatch = useDispatch();

  const { theme } = useContext(ThemeContext);

  if (props.filter === "cart") {
    products = products.filter((product) => inCart.includes(product.article));
  }
  if (props.filter === "favorites") {
    products = products.filter((product) =>
      inFavorites.includes(product.article)
    );
  }

  const setIsInFavorite = (isFavorite, article) => {
    isFavorite
      ? dispatch(deleteFromFavorites(article))
      : dispatch(addToFavorites(article));
  };

  const createModal = (modalData, actionHandlers = []) => {
    const { header, text, closeButton } = modalData;

    const actions = modalData.actions.map((action) => (
      <Button
        backgroundColor={action.backgroundColor}
        text={action.text}
        key={action.key}
        onClick={() => actionHandlers[action.key]}
      />
    ));

    dispatch(openModal({ header, text, closeButton, actions }));
  };
  return (
    <>
      {theme === "cards" ? (
        <ProductList
          products={products}
          cart={inCart}
          favorites={inFavorites}
          setIsInFavorite={setIsInFavorite}
          createModal={createModal}
        />
      ) : (
        <ProductTable
          products={products}
          cart={inCart}
          favorites={inFavorites}
          setIsInFavorite={setIsInFavorite}
          createModal={createModal}
        />
      )}
    </>
  );
}

ProductsWrapper.propTypes = {
  filter: PropTypes.string,
};

ProductsWrapper.defaultProps = {
  filter: "",
};
