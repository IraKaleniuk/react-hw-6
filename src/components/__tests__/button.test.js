import Button from "../button";
import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import renderer from "react-test-renderer";

afterEach(cleanup);

test("snapshot button", () => {
  const buttonProps = {
    backgroundColor: "green",
    text: "submit",
    onClick: function () {
      console.log("submitted");
    },
  };

  const tree = renderer.create(<Button {...buttonProps} />).toJSON();
  expect(tree).toMatchSnapshot();
});

test("should show content", () => {
  const buttonProps = {
    backgroundColor: "green",
    text: "submit",
    onClick: function () {
      console.log("submitted");
    },
  };

  render(<Button {...buttonProps} />);
  expect(screen.getByTestId("button")).toHaveTextContent("submit");
});

test("should have background color", () => {
  const buttonProps = {
    backgroundColor: "green",
    text: "submit",
    onClick: function () {
      console.log("submitted");
    },
  };

  render(<Button {...buttonProps} />);
  expect(screen.getByTestId("button")).toHaveStyle("background-color: green");
});

test("should fire click event", () => {
  const buttonProps = {
    backgroundColor: "green",
    text: "submit",
    onClick: function () {},
  };

  render(<Button {...buttonProps} />);
  fireEvent.click(screen.getByTestId("button"));
});
