import Modal from "../modal";
import { render, screen, cleanup } from "@testing-library/react";
import Button from "../button";
import configureStore from "redux-mock-store";
import { middleware } from "../../redux/middleware";
import { Provider } from "react-redux";

const mockStore = configureStore(middleware);
let store;

beforeEach(() => {
  store = mockStore({
    modalIsOpen: true,
    modalData: {
      header: "Modal header text",
      text: "Modal body text",
      actions: [],
      closeButton: true,
    },
  });
  cleanup();
});

test("Should have close button", () => {
  const modalProps = {
    header: "Modal title",
    text: "Modal text",
    actions: [<Button />, <Button />],
    closeButton: true,
    closeButtonHandler: function () {
      console.log("Modal closed");
    },
  };

  render(<Modal {...modalProps} />);
  expect(screen.getByTestId("closeBtn")).toBeInTheDocument();
});

test("Should not have close button", () => {
  const modalProps = {
    header: "Modal title",
    text: "Modal text",
    actions: [<Button />, <Button />],
    closeButton: false,
    closeButtonHandler: function () {
      console.log("Modal closed");
    },
  };

  render(<Modal {...modalProps} />);
  expect(screen.queryByTestId("closeBtn")).not.toBeInTheDocument();
});

test("Should have buttons", () => {
  const modalProps = {
    header: "Modal title",
    text: "Modal text",
    actions: [<Button text={"Cancel"} />, <Button text={"Ok"} />],
    closeButton: false,
    closeButtonHandler: function () {
      console.log("Modal closed");
    },
  };

  render(<Modal {...modalProps} />);
  expect(screen.getByText("Cancel")).toBeInTheDocument();
  expect(screen.getByText("Ok")).toBeInTheDocument();
});

test("Should have header", () => {
  const modalProps = {
    header: "Modal title",
    text: "Modal text",
    actions: [<Button text={"Cancel"} />, <Button text={"Ok"} />],
    closeButton: false,
    closeButtonHandler: function () {
      console.log("Modal closed");
    },
  };

  render(<Modal {...modalProps} />);
  expect(screen.getByTestId("modalHeader")).toHaveTextContent("Modal title");
});

test("Should have text content", () => {
  const modalProps = {
    header: "Modal title",
    text: "Modal text",
    actions: [<Button text={"Cancel"} />, <Button text={"Ok"} />],
    closeButton: false,
    closeButtonHandler: function () {
      console.log("Modal closed");
    },
  };

  render(<Modal {...modalProps} />);
  expect(screen.getByTestId("modalText")).toHaveTextContent("Modal text");
});
