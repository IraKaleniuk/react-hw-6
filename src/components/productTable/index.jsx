import styles from "./ProductTable.module.scss";
import PropTypes from "prop-types";
import ProductRow from "../productRow";
export default function ProductTable(props) {
  return (
    <table className={styles.table} data-testid="products">
      <thead>
        <tr className={styles.header}>
          <th>Photo</th>
          <th>Name</th>
          <th>Color</th>
          <th>Article</th>
          <th>Price</th>
          <th>Favorites</th>
          <th>Cart</th>
        </tr>
      </thead>
      <tbody>
        {props.products.map((product) => (
          <ProductRow
            key={product.article}
            {...product}
            isFavorite={props.favorites.includes(product.article)}
            inCart={props.cart.includes(product.article)}
            setIsInFavorite={props.setIsInFavorite}
            createModal={props.createModal}
          />
        ))}
      </tbody>
    </table>
  );
}

ProductTable.propTypes = {
  products: PropTypes.array,
  cart: PropTypes.array,
  favorites: PropTypes.array,
  setIsInFavorite: function () {},
  createModal: function () {},
};

ProductTable.defaultProps = {
  cart: [],
  favorites: [],
};
