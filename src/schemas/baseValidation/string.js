import * as Yup from "yup";

export const baseStringValidation = Yup.string()
  .matches(/^[A-Za-zА-ЯҐЄІЇа-яґєії]+$/, "Only letters")
  .min(2, "Min 2 letters required")
  .max(20, "Only 20 letters allowed")
  .required("This field is required");
