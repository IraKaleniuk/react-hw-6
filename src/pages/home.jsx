import { useSelector } from "react-redux";
import Preloader from "../components/preloader";
import ProductsWrapper from "../components/productsWrapper";
import PageHeader from "../components/pageHeader";

export function Home() {
  let { error, loaded } = useSelector((state) => state.products);
  return (
    <>
      <main className="main">
        <section className="container">
          {error ? (
            <>
              <h2 className="title">Something went wrong! :(</h2>
            </>
          ) : null}
          {!loaded && !error ? <Preloader /> : null}
          {loaded && !error ? (
            <>
              <PageHeader />
              <ProductsWrapper />
            </>
          ) : null}
        </section>
      </main>
    </>
  );
}
