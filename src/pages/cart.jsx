import NoProducts from "../components/noProducts";
import { useSelector } from "react-redux";
import BuyProductForm from "../components/buyProductForm";
import styles from "./styles/Cart.module.scss";
import ProductsWrapper from "../components/productsWrapper";
import PageHeader from "../components/pageHeader";

export function Cart() {
  const { inCart: cart } = useSelector((state) => state.cart);

  return (
    <>
      <main className="main">
        <section className="container">
          {cart.length === 0 ? (
            <NoProducts target="cart" />
          ) : (
            <>
              <PageHeader />
              <div className={styles.wrap}>
                <ProductsWrapper filter="cart" />
                <BuyProductForm />
              </div>
            </>
          )}
        </section>
      </main>
    </>
  );
}
